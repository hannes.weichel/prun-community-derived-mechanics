---
title: "Population Infrastructure"
---

## Need Fulfillment
## General Need Fulfillment Formula
`Need Fulfillment = Needs Provided / Needs Needed`

## Needs Needed
The needs needed by a planetary population are a weighted sum of the [current population](#current-population) and the need fulfillment weights listed below.
| Type | Life Support | Safety | Health | Comfort | Culture | Education |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| Pioneer | 0.50 | 0.25 | 0.15 | 0.05 | 0.03 | 0.02 |
| Settler | 0.35 | 0.30 | 0.20 | 0.05 | 0.05 | 0.05 |
| Technician | 0.15 | 0.20 | 0.30 | 0.20 | 0.10 | 0.05 |
| Engineer | 0.10 | 0.10 | 0.15 | 0.35 | 0.20 | 0.10 |
| Scientist | 0.05 | 0.10 | 0.10 | 0.20 | 0.25 | 0.30 |

## Needs Provided
Provided needs come from 3 sources, the fertility of a planet, bases on a planet and population infrastructure.
### Fertility
Planets that are fertile (no matter the percentage) have 100% life support at all times.
### Bases
Bases provide some safety and health for their populations. The habitations on those bases also provide life support. The safety provided by bases is given by:

`Safey = 50 * # of Bases`

The health provided by bases is given by:

`Health = 50 * # of Bases`

Do note that on large planets with many bases added and removed each week, the actual value may differ slightly from this prediced value.
### Population Infrastructure
When fully filled, a small population infrastructure project (SST, INF, etc.) provides 2,500 of their need. A mixed population infrastructure building (EMC, PBH, etc.) provides 1,000 for each need. A large population infrastructure building (SDP, HOS, etc.) provides 5,000 for each need.

The effect of the POPI is directly proportional to the amount of upkeep it receives in a week. All inputs are weighted equally and only count towards fulfillment once consumed on a consumption tick.

For example: An SST provided with only DW (1 out of 3 consumables) for 4 out of 7 days will provide: `1/3 * 4/7 = 4/21` the benefit a full SST would. 

The amount consumed each period can be deduced from the reserve amount. The reserve amount provides enough material for 30 days. In the case of materials consumed every 3 days, divide the reserve by 10 to get the consumption every 3 days. In the case of materials consumed every 10 days, divide the reserve by 3.

POPI upgrades increase in cost by 20% each level, to a maximum of 10 levels on any project.
## Capping of Higher Tier Needs
The types of need fulfillment are split into 4 tiers:

- Life Support
- Safety & Health
- Comfort & Culture
- Education

Needs of a higher tier can only be fulfilled as well as the worst fulfilled need of the tier below.
## Population Happiness
There are two types of happiness, [True Happiness](#true-happiness) and [Averaged Happiness](#averaging-happiness). For this document, whenever Happiness is used on its own, it refers to Averaged Happiness. The Averaged Happiness is the value displayed in the game.
## Averaging Happiness
The happiness displayed is the weighted average of the [True Happiness](#true-happiness) of the previous 5 weeks. The weights are, going from the previous week to 5 weeks ago: `0.4, 0.25, 0.2, 0.1, 0.05`

`H = 0.4 * h0 + 0.25 * h1 + 0.2 * h2 + 0.1 * h3 + 0.05 * h4`
### Missing Data
When not all 5 weeks are available to average, such as in the first 4 weeks since a planet's settlement, the missing terms are ignored and the final result is divided by the sum of the weights that were counted.
## True Happiness
True (unaveraged) happiness is affected by need fulfillment, unemployment, and explorer's grace. It is capped between 0 and 1. In general, it follows this formula:

`h = Weighted Sum of Need Fulfillment + Explorer's Grace - Unemployment`
### Need Fulfillment in Happiness
The table found in [Needs Needed](#needs-needed) provides the weights used in the weighted sum for each population tier.
### Explorer's Grace
The "Explorer's Grace" boon takes place during the first 4 weeks after a new planet is settled. The true happiness of all tiers is increased by 50% (although still capped at 100%) for those 4 weeks. In the case of the formula above, Explorer's Grace is a +50% addition.
### Unemployment
When no open jobs are present, unemployment is listed on the POPR screen. That value is plugged directly into the above formula. That value is calculated as: `# of Unemployed / Current Population`

When open jobs are present, the unemployment rate is given by: `- # of Open Jobs / Current Population` (see [Current Population](#current-population))

The boost from unemployment is capped. The maximum boost given is 30% of the happiness generated from need fulfillment alone.
## Population Growth
Population growth due to happiness is split into 2 types, migration and education.
## Migration 
Migration occurs both in and out. Migration in only occurs for PIO, SET, and TEC, while migration out occurs for all tiers.
### Migration In
Migration in only occurs when `Happiness > 70%` The formula is given by:

`Migration In = Current Population * (Happiness - 0.7)` (see [Current Population](#current-population))
### Migration Out
Migration out occurs when `Happiness < 50%` The formula is given by:

`Migration Out = 0.2 * Current Population * (0.5 - Happiness)` (see [Current Population](#current-population))
## Education
Education growth is when population from one tier leaves and moves up to the next tier. Education growth can only be positive. The formula is given by:

`Education = S * Current Population of Previous Tier * Happiness of Current Tier`

Where S is a constant depending on the tiers involved

| Transition | S |
| ------ | ------ |
| PIO -> SET | 0.015 |
| SET -> TEC | 0.010 |
| TEC -> ENG | 0.005 |
| ENG -> SCI | 0.000 |
### Infrastructure Bonus
Education infrastructure (PBH, LIB, UNI) gives an additive bonus to education growth rates. It provides this bonus regardless of the upkeep status of those projects. The PBH increases the growth rate by 0.0005, the LIB by 0.001, and the UNI by 0.002.

For example, with 1 PBH and 1 UNI on a planet, the ENG growth rate from the table above becomes:

`0.005 + 0.0005 + 0.002 = 0.0075`

## Fractional Population
Fractional population do exist, they are not counted towards the workforce of a player until 1 full worker has been achieved. They do not contribute to education or migration into the next tier.
## Workforce Reserve Pool
The workforce reserve pool is the portion of the workforce that can be pulled by players between POPR. The population reserve is given by:

`Reserve = 0.1 * (Current Population * U + Population Change)` (see [Current Population](#current-population))

Where U is unemployment. If the value is larger that the number of jobs available (such as in the case of open jobs) the reserve pool is not zero, but the formula is unknown.
## Returning Workers to Pool
25% of one's workers return to the pool each POPR to be redistributed. When they are redistributed, it is done in a round-robin style distribution.

## Modeling
Everything up to this point has been about determining the formulas programmed by the developers. This is not sufficient to predict POPI into the future. The main free variables are:

1. The amount of population each player employs
2. The amount of bases on each planet
3. The population infrastructure and upkeep on each planet

All three items are hard to predict. Logarithmic models tend to perform best, but guessing by hand often works better.
## Known Bugs
When building a new population infastructure building, if previous active levels of that building exist, the amount of needs provided at the end of the week is calculated as if all the buildings, including the new inactive one, contributed. For example if 1 SST fed at 100% was present on a planet, and 1 more SST was built that week, the 2nd SST wouldn't consume materials, but at the end of the week, the planet would receive the benefit from 2 SST filled at 100%.
## Definitions
## Current Population 
Current Population is used in this paper to mean the population immediately before the POPR. In the case of SET, TEC, ENG, SCI, this is the population displayed on the previous population report.

In the case of PIO, this is the population displayed on the previous population report plus the number of PIO added or removed via new bases. The current pioneer population is given by:

`Current PIO Population = Next PIO Population - PIO Change = Previous PIO Population + 200 * # of Bases Added or Removed`
