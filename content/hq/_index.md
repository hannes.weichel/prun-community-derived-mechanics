---
title: "HQ"
---

## Upgrade Costs

| HQ Level | Permits To -> From | Part Costs                                                            | Queue Slots |
|----------|--------------------|-----------------------------------------------------------------------|-------------|
| 2        | 2  -> 3            | 6 BBH, 4 BDE, 4 BSE, 2 BTA, 12 MCG, 4 TRU                             | 5           |
| 3        | 3  -> 4            | 16 BBH, 12 BDE, 12 BSE, 6 BTA, 36 MCG, 12 TRU, 20 OFF                 | 5           |
| 4        | 4  -> 5            | 24 BBH, 16 BDE, 16 BSE, 8 BTA, 48 MCG, 8 MFK, 16 TRU, 4 UTS           | 5           |
| 5        | 5  -> 6            | 40 OFF, 20 SUN, 8 UTS, 16 MFK                                         | 7           |
| 6        | 6  -> 7            | 6 LBH, 4 LDE, 4 LSE, 2 LTA, 24 MCG, 12 TRU, 10 POW                    | 7           |
| 7        | 7  -> 8            | 12 LBH, 8 LDE, 8 LSE, 4 LTA, 36 MCG, 16 TRU, 10 SP                    | 7           |
| 8        | 8  -> 9            | 16 LBH, 12 LDE, 12 LSE, 6 LTA, 48 MCG, 20 TRU, 60 OFF, 2 SP           | 7           |
| 9        | 9  -> 10           | 24 LBH, 16 LDE, 16 LSE, 8 LTA, 60 MCG, 24 TRU, 10 SP, 10 POW          | 7           |
| 10       | 10 -> 11           | 100 OFF, 14 SP, 10 POW                                                | 9           |
| 11       | 11 -> 12           | 6 RBH, 4 RDE, 4 RSE, 2 RTA, 48 MCG, 20 TRU, 4 AAR                     | 9           |
| 12       | 12 -> 13           | 12 RBH, 8 RDE, 8 RSE, 4 RTA, 60 MCG, 24 TRU, 2 BWS                    | 9           |
| 13       | 13 -> 14           | 200 OFF, 1 BWS, 1 BMF, 1 AAR                                          | 11          |
| 14       | 14 -> 15           | 16 RBH, 12 RDE, 12 RSE, 6 RTA, 100 MCG, 28 TRU, 2 BWS, 2 AAR          | 11          |
| 15       | 15 -> 16           | 24 RBH, 16 RDE, 16 RSE, 8 RTA, 150 MCG, 32 TRU, 2 BWS 2 BMF           | 11          |
| 16       | 16 -> 17           | 300 OFF, 1 LOG                                                        | 14          |
| 17       | 17 -> 18           | 12 ABH, 8 ADE, 8 ASE, 4 ATA, 200 MCG, 40 TRU, 1 COM                   | 14          |
| 18       | 18 -> 19           | 1 ADS, 400 OFF                                                        | 17          |
| 19       | 19 -> 20           | 24 ABH, 16 ADE, 1 ADS, 16 ASE, 8 ATA, 1 COM, 1 LOG, 500 MCG, 80 TRU   | 17          |
| 20       | 20 -> 21           | 1 ADS, 1 COM, 1 LOG, 500 OFF                                          | 20          |
| 21       | 21 -> 22           | 51 ABH, 34 ADE, 2 ADS, 34 ASE, 17 ATA, 2 COM, 2 LOG, 214 MCG, 85 TRU  | 20          |
| 22       | 22 -> 23           | 79 ABH, 53 ADE, 2 ADS, 53 ASE, 26 ATA, 2 COM, 2 LOG, 331 MCG, 132 TRU | 20          |


## HQ Bonus

The multiplier to the base efficiency bonus provided by the HQ can be given by:

`Multiplier = -2 * (UsedPermits / TotalPermits) + 3`
